package mati.xmlparser;

import com.sun.istack.internal.NotNull;
import mati.simplelogger.SimpleLogger;
import mati.tools.Converter;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.*;
import java.net.URL;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.logging.Logger;

/**
 * @author Mateusz Paluch
 */
public class XmlParser extends Converter<InputStream, Node, XmlParser> {
	protected static final Logger log = SimpleLogger.getLogger(XmlParser.class.getName());

	public enum EventKind { Node, Attribute };

	public class Event {
		public Event() {}
		public Event(EventKind kind, String message) {
			this.kind = kind;
			this.message = message;
		}
		public EventKind kind;
		public String message;
	}

	private Node current;
	private BiConsumer<StartElement, Consumer<Attribute>> attributeLooper;
	private Thread t;
	private long eventDuration;
	private long initDuration;
	private long duration;

	{
		attributeLooper = (element, consumer) -> {
			Iterator<Attribute> it = element.getAttributes();
			while (it.hasNext()) {
				Attribute a = it.next();
				consumer.accept(a);
			}
		};
	}

	@Override
	protected XmlParser getMe() {
		return this;
	}

	public synchronized final XmlParser from(@NotNull URL url) {
		InputStream source = null;
		try {
			source = url.openStream();
		} catch (IOException e) {
			log.severe(e.getLocalizedMessage());
			if (source != null) {
				try {
					source.close();
				} catch (IOException e1) {
					log.severe(e1.getLocalizedMessage());
				}
				source = null;
			}
		}
		from(source);
		return this;
	}

	public synchronized final XmlParser from(@NotNull String text) {
		InputStream source = new InputStream() {
			int pos = 0;
			@Override
			public int read() throws IOException {
				if (pos < text.length()) {
					char c = text.charAt(pos);
					pos += 1;
					return c;
				} else {
					return -1;
				}
			}
		};
		from(source);
		return this;
	}

	@Override
	protected synchronized final XmlParser onProcess() {
		if (t != null) {
			t.start();
		} else {
			silentRunner();
		}
		result = current;
		return this;
	}

	private void silentRunner() {
		try {
			processNormal();
		} catch (IOException | XMLStreamException e) {
			log.severe(e.getLocalizedMessage());
		}
	}

	private void processNormal() throws IOException, XMLStreamException {
		long start = System.currentTimeMillis();

		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLEventReader reader = factory.createXMLEventReader(source);

		initDuration = System.currentTimeMillis() - start;

		current = Node.createRoot();

		while(reader.hasNext()){
			eventDuration = System.currentTimeMillis();
			XMLEvent event = reader.nextEvent();

			switch (event.getEventType()) {
				case XMLEvent.START_ELEMENT:
					startElement(event.asStartElement());
					break;
				case XMLEvent.END_ELEMENT:
					current = current.parent();
					break;
			}
		}
		source.close();

		duration = System.currentTimeMillis() - start;
	}

	private void startElement(StartElement element) {
		String name = element.getName().getLocalPart();

		current = current.create(name);
		Iterator<Attribute> it = element.getAttributes();
		while (it.hasNext()) {
			Attribute a = it.next();
			current.set(a.getName().getLocalPart(), a.getValue());
		}
		eventDuration = System.currentTimeMillis() - eventDuration;
	}

	public long getDuration() {
		return duration;
	}

	public long getInitializationDuration() {
		return initDuration;
	}

	public synchronized void waitForWorker() throws InterruptedException {
		if (t != null) {
			t.join();
		}
		t = null;
	}

}
