package mati.xmlparser;

import java.util.Collection;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * @auhor Mateusz Paluch
 */
public class NodeWalker {
	private Node current;
	private Consumer<Node> worker;
	private BiConsumer<Node, Integer> depthWorker;
	private Integer depth;

	public static final Consumer<Node> printer;

	static {
		printer = node -> {
			System.out.println(node.name());
		};
	}

	public NodeWalker tree(Node node) {
		current = node;
		return this;
	}

	public NodeWalker worker(Consumer<Node> worker) {
		this.worker = worker;
		return this;
	}

	public NodeWalker worker(BiConsumer<Node, Integer> worker) {
		this.depthWorker = worker;
		depth = 0;
		return this;
	}

	public NodeWalker process() {
		if (depthWorker != null) {
			depthProcess(current);
		} else {
			process(current);
		}
		return this;
	}

	private void depthProcess(Node node) {
		depthWorker.accept(node, depth);
		depth += 1;
		Collection<Node> c = node.children();
		//if (c != null) {
			c.forEach(this::depthProcess);
		//}
		depth -= 1;
	}

	private void process(Node node) {
		worker.accept(node);
		Collection<Node> c = node.children();
		//if (c != null) {
			c.forEach(this::process);
		//}
	}

	public NodeWalker close() {
		current = null;
		worker = null;
		depthWorker = null;
		return this;
	}
}
